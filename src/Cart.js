export default class Cart {

    constructor() {
        this.items = [];
    }

    add(shoppingItem) {
        this.items.push(shoppingItem);
    }

    getTotalPrice = (discount = price => 0) => {
        let totalPrice = this.items.reduce((result, shoppingItem) => result + shoppingItem.getTotalPrice(), 0);

        return totalPrice - discount(totalPrice);
    };

    getVipItems = () => this.items.filter(item => item.getPrice() >=20);

    allItems = () => this.items.map(item => `${item.getQty()}x ${item.getName()}`);
}