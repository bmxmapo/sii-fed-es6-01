/*
    describe() - zbiór testów
    it() - przypadek testowy

    żeby coś zaimportować z pliku, musi on coś eksportować (ES6). Można eksportować domyślnie lub po nazwie {}. 1 plik - max 1 default export.

    jak importujesz dwie funkcje o tej samej nazwie, może użyć as, żeby przypisać je do lokalnych zmiennych. Dla eksportów domyślnych, nazwa w
    importcie to lokalny alias, możesz to nazwać jak chcesz.

 */

// import {foo as function1} from './ShoppingItem';
// import {foo as function2} from './SomethingElse';

import ShoppingItem from "./ShoppingItem";

describe("ShoppingItem", function() {
    it("should return product name", function() {
        // given
        var shoppingItem = new ShoppingItem("apple");

        // when
        var result = shoppingItem.getName();

        // then
        expect(result).toBe("apple");
    });

    it("should return total price", function() {
        // given
        var shoppingItem = new ShoppingItem("apple", 4, 2.5)

        // when
        var result = shoppingItem.getTotalPrice();

        // then
        expect(result).toBe(10);
    })

});
