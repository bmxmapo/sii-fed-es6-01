/*
    ZD: Przerobić to na nowocześniejszą składnie ala ES6 + funkcje strzałkowe
    ZD: Niech zwraca listę produktów

 */

module.exports = function Cart() {

    this.items = [];

    this.add = function (shoppingItem) {
        this.items.push(shoppingItem)
    };

    /* tak się definiuje wartości defaultowe */

    this.getTotalPrice = function (discount = price => 0) {

        /*
        var result = 0;

        for (var i = 0; i < this.items.length; i++) {
            var item = this.items[i];

            result += item.getTotalPrice();
        }
        this.items.forEach(function(item) {
            result += item.getTotalPrice();
        })
        */

        var totalPrice = this.items.reduce(function (result, shoppingItem) {
            return result + shoppingItem.getTotalPrice();
        }, 0);

        var priceWithDiscount = totalPrice - discount(totalPrice);

        return priceWithDiscount;
    };

    this.getVipItems = function() {
        /*
        this.items.forEach(function(item) {
            if (item.getPrice() >= 20){
                result.push(item);
            }
        })
        */

        // Filter zwraca tablicę zawierającą wszystkie item'y, dla których callback zwraca true


        return this.items.filter(function(item) {
            return item.getPrice() >= 20;
        });
    };

    // Zapis za pomocą funkcji strzałkowej (to samo co wyżej):

    this.getVipItemsNew = () => this.items.filter(item => item.getPrice() >=20);
};