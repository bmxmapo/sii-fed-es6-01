//export function(foo) {};

export default class ShoppingItem {

    // constructor jest defaultowy dla wszystkich "class"

    constructor(name, qty, price) {
        this.name = name;
        this.qty = qty;
        this.price = price;
    }

    getName() {
        return this.name;
    }

    getQty() {
        return this.qty;
    }

    getPrice() {
        return this.price;
    }

    getTotalPrice() {
        return this.qty * this.price;
    }

}